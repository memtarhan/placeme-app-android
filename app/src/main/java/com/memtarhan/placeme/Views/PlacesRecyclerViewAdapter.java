package com.memtarhan.placeme.Views;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.memtarhan.placeme.Controllers.PlacesFragment.OnListFragmentInteractionListener;
import com.memtarhan.placeme.Models.Place;
import com.memtarhan.placeme.R;
import com.memtarhan.placeme.Services.ImageDownloader;

import java.util.ArrayList;


public class PlacesRecyclerViewAdapter extends RecyclerView.Adapter<PlacesRecyclerViewAdapter.ViewHolder> {

    private final OnListFragmentInteractionListener listener;
    private ArrayList<Place> places;

    public PlacesRecyclerViewAdapter(ArrayList<Place> places, OnListFragmentInteractionListener listener) {
        this.places = places;
        this.listener = listener;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_home, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.place = places.get(position);
        holder.nameView.setText(places.get(position).name);
        holder.vicinityView.setText(places.get(position).vicinity);
        holder.ratingView.setText(String.valueOf(places.get(position).rating));

        boolean openNow = places.get(position).openNow;

        final String _OPEN = "Open";
        final String _CLOSE = "Close";
        if (openNow) {

            holder.openNowView.setText(_OPEN);
            holder.openNowView.setTextColor(Color.GREEN);

        } else {

            holder.openNowView.setText(_CLOSE);
            holder.openNowView.setTextColor(Color.RED);
        }


        new ImageDownloader(holder.iconImageView).execute(places.get(position).icon);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    listener.onListFragmentInteraction(holder.place);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return places.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nameView;
        public final TextView vicinityView;
        public final TextView ratingView;
        public final TextView openNowView;
        public final ImageView iconImageView;

        public Place place;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nameView = view.findViewById(R.id.name);
            vicinityView = view.findViewById(R.id.vicinity);
            ratingView = view.findViewById(R.id.rating);
            openNowView = view.findViewById(R.id.open_now);
            iconImageView = view.findViewById(R.id.icon_image);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + nameView.getText() + "'";
        }
    }
}
