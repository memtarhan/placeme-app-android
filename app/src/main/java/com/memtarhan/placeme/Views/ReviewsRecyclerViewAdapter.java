package com.memtarhan.placeme.Views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.memtarhan.placeme.Controllers.ReviewsFragment.OnListFragmentInteractionListener;
import com.memtarhan.placeme.Models.Review;
import com.memtarhan.placeme.R;
import com.memtarhan.placeme.Services.ImageDownloader;

import java.util.ArrayList;


public class ReviewsRecyclerViewAdapter extends RecyclerView.Adapter<ReviewsRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<Review> reviews;
    private final OnListFragmentInteractionListener mListener;

    public ReviewsRecyclerViewAdapter(ArrayList<Review> reviews, OnListFragmentInteractionListener listener) {
        this.reviews = reviews;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_reviews, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.review = reviews.get(position);

        holder.nameView.setText(reviews.get(position).authorName);
        holder.contentView.setText(reviews.get(position).text);
        holder.ratingView.setText(String.valueOf(reviews.get(position).rating));
        holder.timeView.setText(reviews.get(position).timeDescription);

        ImageDownloader task = (ImageDownloader) new ImageDownloader(holder.profileView).execute(reviews.get(position).profilePhoto);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.review);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nameView;
        public final TextView contentView;
        public final TextView ratingView;
        public final TextView timeView;
        public final ImageView profileView;

        public Review review;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nameView = view.findViewById(R.id.name);
            contentView = view.findViewById(R.id.text);
            ratingView = view.findViewById(R.id.rating);
            timeView = view.findViewById(R.id.time);
            profileView = view.findViewById(R.id.profile_photo);

        }

    }
}
