package com.memtarhan.placeme.Views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.memtarhan.placeme.Controllers.PhotosFragment.OnListFragmentInteractionListener;
import com.memtarhan.placeme.R;
import com.memtarhan.placeme.Services.ImageDownloader;
import com.memtarhan.placeme.Utilities.DataURL;

import java.util.ArrayList;


public class PhotosRecyclerViewAdapter extends RecyclerView.Adapter<PhotosRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<String> photos;
    private final OnListFragmentInteractionListener mListener;

    public PhotosRecyclerViewAdapter(ArrayList<String> photos, OnListFragmentInteractionListener listener) {
        this.photos = photos;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_photos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.photo = photos.get(position);

        DataURL.Photo.photoReference = photos.get(position);
        String _URL = DataURL.Photo.getURL();

        ImageDownloader task = (ImageDownloader) new ImageDownloader(holder.photoView).execute(_URL);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.photo);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView photoView;

        public String photo;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            photoView = view.findViewById(R.id.photo);
        }

    }
}
