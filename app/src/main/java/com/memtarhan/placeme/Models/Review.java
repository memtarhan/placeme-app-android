package com.memtarhan.placeme.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by memtarhan on 11/18/17.
 */

public class Review {

    public String authorName;
    public String profilePhoto;
    public String timeDescription;
    public String text;
    public double rating;


    public Review(JSONObject data) throws JSONException {

        this.authorName = data.getString("author_name");
        this.profilePhoto = data.getString("profile_photo_url");
        this.timeDescription = data.getString("relative_time_description");
        this.text = data.getString("text");
        this.rating = data.getDouble("rating");
    }

    @Override
    public String toString() {
        return "Review{" +
                "authorName='" + authorName + '\'' +
                ", profilePhoto='" + profilePhoto + '\'' +
                ", timeDescription='" + timeDescription + '\'' +
                ", rating=" + rating +
                '}';
    }
}
