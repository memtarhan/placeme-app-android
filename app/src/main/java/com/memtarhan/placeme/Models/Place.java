package com.memtarhan.placeme.Models;

import android.location.Location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by memtarhan on 11/17/17.
 */

public class Place {

    public static Place shared = new Place();

    public String id;
    public String name;
    public String vicinity;
    public String icon;
    public String website;
    public String phoneNumber;
    public double rating;
    public boolean openNow;
    public ArrayList<String> types;
    public ArrayList<String> photos;
    public ArrayList<Review> reviews;
    public Location location;

    private Place() {

    }

    public Place(JSONObject data) throws JSONException {

        this.types = new ArrayList<>();
        this.photos = new ArrayList<>();
        this.reviews = new ArrayList<>();

        this.id = data.getString("place_id");
        this.name = data.getString("name");
        this.vicinity = data.getString("vicinity");
        this.icon = data.getString("icon");

        JSONArray types = data.getJSONArray("types");
        for (int i = 0; i < types.length(); i++) {
            this.types.add((String) types.get(i));
        }

        try {

            this.rating = data.getDouble("rating");

        } catch (JSONException e) {

            this.rating = 0.0;
        }

        try {

            JSONObject openingHours = data.getJSONObject("opening_hours");
            this.openNow = openingHours.getBoolean("open_now");

        } catch (JSONException e) {

            this.openNow = false;
        }

        try {

            JSONArray photos = data.getJSONArray("photos");
            for (int i = 0; i < photos.length(); i++) {
                JSONObject photo = photos.getJSONObject(i);
                this.photos.add(photo.getString("photo_reference"));
            }

        } catch (JSONException e) {

        }

        try {

            JSONObject geometry = data.getJSONObject("geometry");
            JSONObject location = geometry.getJSONObject("location");

            double lat = location.getDouble("lat");
            double lng = location.getDouble("lng");

            Location l = new Location("");
            l.setLatitude(lat);
            l.setLongitude(lng);
            this.location = l;

        } catch (JSONException e) {

            this.location = null;
        }

        try {

            JSONArray reviews = data.getJSONArray("reviews");
            for (int i = 0; i < reviews.length(); i++) {
                Review review = new Review(reviews.getJSONObject(i));
                this.reviews.add(review);
            }

        } catch (JSONException e) {

        }

        try {

            this.website = data.getString("website");

        } catch (JSONException e) {

        }

        try {

            this.phoneNumber = data.getString("international_phone_number");

        } catch (JSONException e) {

        }
    }

    @Override
    public String toString() {
        return "Place{" +
                "photos=" + photos.size() +
                ", reviews=" + reviews +
                '}';
    }
}
