package com.memtarhan.placeme.Services;

import android.os.AsyncTask;
import android.util.Log;

import com.memtarhan.placeme.Models.Place;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by memtarhan on 11/18/17.
 */

public class FetchPlaceDetails extends AsyncTask<String, Void, String> {

    public Place place;
    private onPlaceDetailsListener listener;

    public FetchPlaceDetails() {
        listener = null;
    }

    public void setListener(onPlaceDetailsListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... urlStrings) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String jsonStr = null;

        try {
            URL weatherURL = new URL(urlStrings[0]);
            urlConnection = (HttpURLConnection) weatherURL.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            if (inputStream != null) {
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                if (buffer.length() != 0) {
                    jsonStr = buffer.toString();
                }
            }
        } catch (IOException e) {
            Log.e("MainActivity", "Error ", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("MainActivity", "Error closing stream", e);
                }
            }
        }

        return jsonStr;
    }

    @Override
    protected void onPostExecute(String jsonStr) {
        super.onPostExecute(jsonStr);

        try {

            JSONObject data = new JSONObject(jsonStr);
            JSONObject result = data.getJSONObject("result");

            Place place = new Place(result);
            this.place = place;
            System.out.println("Number Of Photos: " + place.photos.size());
            listener.place(place);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public interface onPlaceDetailsListener {

        void place(Place place);
    }
}
