package com.memtarhan.placeme.Controllers;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;

import com.memtarhan.placeme.Models.Place;
import com.memtarhan.placeme.R;
import com.memtarhan.placeme.Services.FetchPlaces;
import com.memtarhan.placeme.Utilities.DataURL;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements PlacesFragment.OnListFragmentInteractionListener, SearchView.OnQueryTextListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        getSupportActionBar().setTitle("Search");

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = findViewById(R.id.search_view);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        System.out.println("onQueryTextSubmit: " + query);

        DataURL.Nearby.type = query;
        DataURL.Nearby.keyword = query;
        String _URL = DataURL.Nearby.getURL();

        FetchPlaces fetchPlaces = new FetchPlaces();
        fetchPlaces.execute(_URL);

        fetchPlaces.setListener(new FetchPlaces.onPlacesListener() {
            @Override
            public void places(ArrayList<Place> places) {

                PlacesFragment homeFragment = new PlacesFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();

                homeFragment.places = places;
                fragmentManager.beginTransaction().replace(R.id.frame_layout, homeFragment).commit();
            }
        });

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        System.out.println("onQueryTextChange: " + newText);
        return false;
    }


    @Override
    public void onListFragmentInteraction(Place place) {

        Place.shared = place;
        startActivity(new Intent(this, DetailsActivity.class));
    }

}
