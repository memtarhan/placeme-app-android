package com.memtarhan.placeme.Controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.memtarhan.placeme.Models.Place;
import com.memtarhan.placeme.Models.Review;
import com.memtarhan.placeme.R;
import com.memtarhan.placeme.Services.FetchPlaceDetails;
import com.memtarhan.placeme.Utilities.DataURL;

public class DetailsActivity extends AppCompatActivity implements PhotosFragment.OnListFragmentInteractionListener, ReviewsFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportActionBar().setTitle("Details");

        DataURL.Details.placeId = Place.shared.id;
        String _URL = DataURL.Details.getURL();

        FetchPlaceDetails fetchPlaceDetails = new FetchPlaceDetails();
        fetchPlaceDetails.execute(_URL);

        fetchPlaceDetails.setListener(new FetchPlaceDetails.onPlaceDetailsListener() {
            @Override
            public void place(Place place) {

                // Setting UI

                Place.shared = place;

                TextView nameView = findViewById(R.id.name);
                nameView.setText(place.name);

                TextView vicinityView = findViewById(R.id.vicinity);
                vicinityView.setText(place.vicinity);

                TextView phoneNumberView = findViewById(R.id.phone_number);
                phoneNumberView.setText(place.phoneNumber);

                TextView ratingView = findViewById(R.id.rating);
                ratingView.setText(String.valueOf(place.rating));

                StringBuilder t = new StringBuilder();
                for (String type : place.types) {
                    t.append(type + ", ");
                }

                TextView typesView = findViewById(R.id.types);
                typesView.setText(t.toString());

                PhotosFragment photosFragment = new PhotosFragment();
                ReviewsFragment reviewsFragment = new ReviewsFragment();

                FragmentManager fragmentManager = getSupportFragmentManager();

                fragmentManager.beginTransaction().replace(R.id.photos_frame_layout, photosFragment).commit();
                fragmentManager.beginTransaction().replace(R.id.reviews_frame_layout, reviewsFragment).commit();

            }
        });


    }

    @Override
    public void onListFragmentInteraction(String photoURL) {

        Intent intent = new Intent(this, PhotoActivity.class);
        intent.putExtra("photoReference", photoURL);

        startActivity(intent);
    }

    @Override
    public void onListFragmentInteraction(Review review) {

    }
}
