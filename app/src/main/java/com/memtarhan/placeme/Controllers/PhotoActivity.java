package com.memtarhan.placeme.Controllers;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.memtarhan.placeme.R;
import com.memtarhan.placeme.Services.ImageDownloader;
import com.memtarhan.placeme.Utilities.DataURL;

public class PhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        ImageView imageView = findViewById(R.id.photo_view);

        Bundle bundle = getIntent().getExtras();
        String ref = bundle.getString("photoReference");

        DataURL.Photo.photoReference = ref;
        String _URL = DataURL.Photo.getURL();

        ImageDownloader task = (ImageDownloader) new ImageDownloader(imageView).execute(_URL);
    }
}
