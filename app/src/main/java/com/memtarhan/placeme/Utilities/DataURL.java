package com.memtarhan.placeme.Utilities;

import com.memtarhan.placeme.Models.User;

/**
 * Created by memtarhan on 11/18/17.
 */

public class DataURL {

    private static String _API_KEY = "AIzaSyDaV0S_eZD1JYLnNwC2NR5_w7OD-wQbjTM";
    private static String _PREFIX_NEARBY_SEARCH = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
    private static String _PREFIX_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json?";
    private static String _PREFIX_PHOTO = "https://maps.googleapis.com/maps/api/place/photo?";

    public static class Nearby {

        public static String type = "";
        public static String keyword = "";
        public static int radius = 500;

        public static String getURL() {

            StringBuilder builder = new StringBuilder();

            builder.append(_PREFIX_NEARBY_SEARCH);
            builder.append("location=" + User.profile.location.getLatitude() + "," + User.profile.location.getLongitude());
            builder.append("&radius=" + radius);

            if (type.length() > 0) {
                builder.append("&type=" + type);
            }

            if (keyword.length() > 0) {
                builder.append("&keyword" + keyword);
            }

            builder.append("&key=" + _API_KEY);

            return builder.toString();
        }
    }

    public static class Details {

        public static String placeId;

        public static String getURL() {

            StringBuilder builder = new StringBuilder();

            builder.append(_PREFIX_DETAILS);
            builder.append("placeid=" + placeId);
            builder.append("&key=" + _API_KEY);

            return builder.toString();
        }
    }

    public static class Photo {

        public static String photoReference;
        public static int maxWidth = 1000;

        public static String getURL() {

            StringBuilder builder = new StringBuilder();

            builder.append(_PREFIX_PHOTO);
            builder.append("maxwidth=" + maxWidth);
            builder.append("&photoreference=" + photoReference);
            builder.append("&key=" + _API_KEY);

            return builder.toString();
        }
    }
}
